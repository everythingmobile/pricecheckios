//
//  ThirdViewController.m
//  PriceCheckIos
//
//  Created by Pravin Gadakh on 2/28/14.
//  Copyright (c) 2014 Everythingmobile. All rights reserved.
//

#import "ThirdViewController.h"
#import "ProductViewModel.h"
#import "XYZViewController.h"
@interface ThirdViewController ()
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ThirdViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"here in tv");
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"number of rows");
    return 1;
}
-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)shouldAutorotate {
    NSLog(@"ShouldAutorotate!!");
    return YES;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"row count %lu",(unsigned long)[[ProductViewModel sharedProductViewModel] recentItems].count);
    return [[ProductViewModel sharedProductViewModel] recentItems].count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *recentItems=[[ProductViewModel sharedProductViewModel] recentItems];
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text=recentItems[indexPath.row];
    return cell;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"RecentSegue"])
    {
        NSMutableArray *recentItems=[[ProductViewModel sharedProductViewModel] recentItems];
        XYZViewController *destinationViewController=(XYZViewController *)[[[segue destinationViewController] viewControllers]objectAtIndex:0];
        NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
        ProductViewModel *viewModel=[ProductViewModel sharedProductViewModel];
        viewModel.searchText=recentItems[indexPath.row];
        [destinationViewController searchForProduct:recentItems[indexPath.row]];
    }
}
@end
