//
//  Utility.h
//  PriceCheckIos
//
//  Created by Pravin Gadakh on 3/23/14.
//  Copyright (c) 2014 Everythingmobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+(UIInterfaceOrientation) getDeviceOrientation;
+(CGFloat) getDeviceWidth;
+(CGFloat) getDeviceHeight;
+(CGPoint) getDeviceCenter;

@end
