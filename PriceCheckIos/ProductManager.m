//
//  ProductManager.m
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import "ProductManager.h"
#import "ProductRequestClient.h"
#import "Product.h"
#import "ProductViewModel.h"
@interface ProductManager()
@property ProductRequestClient *productRequestClient;
@end;
@implementation ProductManager
+(instancetype)sharedManager
{
    static id _sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}
-(id)init
{
    if(self=[super init])
    {
        self.productRequestClient=[[ProductRequestClient alloc]init];
    }
    return self;
}
-(RACSignal *)search:(NSString *)name page:(NSString *)page
{
    NSString *stringToSearch = [name stringByReplacingOccurrencesOfString:@" +" withString:@"+"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, name.length)];
    NSString *url=[NSString stringWithFormat:@"http://ec2-54-255-169-135.ap-southeast-1.compute.amazonaws.com:8080/CompareNBuy/products/?q=%@&page=%@",stringToSearch,page];
    return [self.productRequestClient getProductsFromUrlWithString:url];
}
-(NSMutableArray *)sortByPrice:(NSMutableArray *)productList withOrder:(int)order
{
    NSArray *sortedArray = [productList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *firstPrice=[(Product *)a price];
        NSString *secondPrice=[(Product *)b price];
        firstPrice=[firstPrice stringByReplacingOccurrencesOfString:@"Rs. " withString:@""];
        secondPrice=[secondPrice stringByReplacingOccurrencesOfString:@"Rs. " withString:@""];
        firstPrice=[firstPrice stringByReplacingOccurrencesOfString:@"," withString:@""];
        secondPrice=[secondPrice stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSInteger first =[firstPrice integerValue];
        NSInteger second = [secondPrice integerValue];
        if(!order)
        {
            
            if(first > second)return NSOrderedDescending;
            else if(first < second)return NSOrderedAscending;
            return NSOrderedSame;
        }
        else
        {
            
            if(first < second)return NSOrderedDescending;
            else if(first > second)return NSOrderedAscending;
            return NSOrderedSame;
        }
    }];
    
    return [NSMutableArray arrayWithArray:sortedArray];
}
-(RACSignal *)getAutocompleteResults:(NSString *)name
{
    NSString *stringToSearch = [name stringByReplacingOccurrencesOfString:@" +" withString:@"+"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, name.length)];
    NSString *search=[NSString stringWithFormat:@"http://www.flipkart.com/s?query=%@&vertical=search.flipkart.com",stringToSearch];
    return [[self.productRequestClient getProductsFromUrlWithString:search] map:^id(id x) {
        if([x isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dict=(NSDictionary *)x;
            NSArray *arr=[dict objectForKey:stringToSearch];
            return arr[1];
        }
        return nil;
    }];
}
-(NSMutableArray *)filterBySite:(NSMutableArray *)productList
{
    ProductViewModel *viewModel=[ProductViewModel sharedProductViewModel];
    RACSequence *filteredArray= [productList.rac_sequence filter:^BOOL(Product *product) {
        return [[viewModel.sitesFilter objectForKey:[product.siteName lowercaseString]] boolValue];
    }];
    NSMutableArray *filteredProducts=[[NSMutableArray alloc]initWithArray:filteredArray.array];

    return filteredProducts;
    
}
-(void)saveRecentItems
{
    NSString *documentsDirectory = [NSHomeDirectory()
                                    stringByAppendingPathComponent:@"Documents"];
    NSString *filePath =[documentsDirectory stringByAppendingPathComponent:@"recentItems.txt"];
    NSMutableArray *recentItems=[[ProductViewModel sharedProductViewModel]recentItems];
    if(recentItems.count>20)
    {
        recentItems=[NSMutableArray arrayWithArray:[recentItems subarrayWithRange:NSMakeRange(0,20)]];
        [ProductViewModel sharedProductViewModel].recentItems=recentItems;
    }
    [recentItems writeToFile:filePath atomically:YES];
}

-(NSMutableArray *)filterProducts:(NSMutableArray *)productList bySuggestion:(BOOL)suggested
{
    RACSequence *filteredArray=[productList.rac_sequence filter:^BOOL(Product *product) {
        return  (product.autoSuggested == suggested);
    }];
    NSMutableArray *filteredProducts=[[NSMutableArray alloc]initWithArray:filteredArray.array];
    return filteredProducts;
}
@end