//
//  ProductRequestClient.h
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa/ReactiveCocoa.h>
@interface ProductRequestClient : NSObject
-(RACSignal *) fetchJsonFromUrlWithString:(NSString *)Url;
-(RACSignal *) getProductsFromUrlWithString:(NSString *)Url;
@end
