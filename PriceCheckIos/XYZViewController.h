//
//  XYZViewController.h
//  SimpleTableTest
//
//  Created by twilight on 04/12/1935 SAKA.
//  Copyright (c) 1935 SAKA twilight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBKenBurnsView.h"

@interface XYZViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,JBKenBurnsViewDelegate> {
    JBKenBurnsView *kenView;
}
@property (nonatomic, retain) IBOutlet JBKenBurnsView *kenView;
-(void)searchForProduct:(NSString *)searchText;
@end
