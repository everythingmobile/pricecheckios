//
//  ProductRequestClient.m
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import "ProductRequestClient.h"
#import "Product.h"
@interface ProductRequestClient()
@property (nonatomic,strong) NSURLSession *session;
@end
@implementation ProductRequestClient
@synthesize session=_session;
-(NSURLSession *)session
{
    if(!_session)
    {
        NSURLSessionConfiguration *config=[NSURLSessionConfiguration defaultSessionConfiguration];
        _session=[NSURLSession sessionWithConfiguration:config];
    }
    return _session;
}
-(RACSignal *)getProductsFromUrlWithString:(NSString *)Url
{
    
    NSDictionary *siteNames=@{
                @"AmazonResponse":@"Amazon",
                @"FlipkartResponse":@"flipkart",
                @"HomeShop18Response":@"homeshop18",
                @"TradusResponse":@"tradus",
                @"NaaptolResponse":@"naaptol",
                @"InfibeamResponse":@"infibeam",
                @"EbayResponse":@"ebay",
                @"SnapdealResponse":@"snapdeal"
                };
    return [[self fetchJsonFromUrlWithString:Url]map:^id(id json) {
        if([json isKindOfClass:[NSArray class]])
        {
            NSMutableArray *products=[[NSMutableArray alloc]init];
            NSArray *siteResponses=(NSArray *)json;
            for(id element in siteResponses)
            {
                if([element isKindOfClass:[NSDictionary class]])
                {
                    
                    NSDictionary *dict=(NSDictionary *)element;
                    NSString *site= [dict objectForKey:@"siteName"];
                    NSArray *productList=(NSArray*)[dict objectForKey:@"products"];
                    //NSLog(@"%@",site);
                    for(id productElement in productList)
                    {
                        
                        NSString *name=[productElement objectForKey:@"name"];
                        NSString *price=[productElement objectForKey:@"price"];
                        NSString *imageUrl=[productElement objectForKey:@"imageUrl"];
                        NSString *productLink=[productElement objectForKey:@"productLink"];
                        NSString *siteName=[siteNames objectForKey:[productElement objectForKey:@"siteName"]];
                        BOOL suggested=NO;
                        if([site isEqualToString:@"AutoSuggest"]==YES)
                        {
                                suggested=YES;
                        }
                        Product *product=[[Product alloc]initWithName:name Price:price Site:siteName ImageUrl:imageUrl ProductLink:productLink AutoSuggest:suggested];
                        //NSLog(@"%@",product.name);
                        if(product){
                            [products addObject:product];
                            
                        }
                        
                    }
                }
            }
            return products;
        }
        else return json;
        
    }];
}
-(RACSignal *)fetchJsonFromUrlWithString:(NSString *)Url
{
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURL *prodURL=[NSURL URLWithString:Url];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: prodURL];
        [request addValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36" forHTTPHeaderField:@"User-Agent"];
        NSURLSession *session = self.session;
        NSURLSessionDataTask *task=[session dataTaskWithRequest:request
                                              completionHandler:^(NSData *data,NSURLResponse *res,NSError *error){
                                                  if(!error)
                                                  {
                                                      NSError *jsonError=nil;
                                                      id json=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
                                                      if(!jsonError)
                                                          [subscriber sendNext:json];
                                                      else [subscriber sendError:jsonError];
                                                  }
                                                  else [subscriber sendError:error];
                                                  
                                                  [subscriber sendCompleted];
                                              }];
        
        
        [task resume];
        
        return [RACDisposable disposableWithBlock:^(){
            [task cancel];
        }];
        
    }]doError:^(NSError *error) {
        NSLog(@"Error Occured in Creating Signal");
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"errorRetrievingProduct"
         object:self];
    }];
}
@end
