//
//  ProductViewModel.m
//  PriceCheckIos
//
//  Created by twilight on 17/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import "ProductViewModel.h"

@implementation ProductViewModel
+(instancetype)sharedProductViewModel
{
    static id _sharedProductViewModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedProductViewModel = [[self alloc] init];
    });
    
    return _sharedProductViewModel;
}
-(id)init
{
    if(self=[super init])
    {
        NSDictionary *dict=@{@"amazon":[NSNumber numberWithBool:YES] ,@"flipkart":[NSNumber numberWithBool:YES] ,@"homeshop18":[NSNumber numberWithBool:YES] ,@"tradus":[NSNumber numberWithBool:YES] ,@"naaptol":[NSNumber numberWithBool:YES] ,
                           @"infibeam":[NSNumber numberWithBool:YES] ,@"ebay":@1,@"snapdeal":[NSNumber numberWithBool:YES] };
        self.sitesFilter=[dict mutableCopy];
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *documentsDirectory = [NSHomeDirectory()
                                        stringByAppendingPathComponent:@"Documents"];
        NSString *filePath =[documentsDirectory stringByAppendingPathComponent:@"recentItems.txt"];
        NSError *error;
        BOOL fileExists=[fileMgr fileExistsAtPath:filePath isDirectory:NO];
        if(!fileExists)
        {
            [@"" writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        }
        self.recentItems=[[NSMutableArray alloc]initWithContentsOfFile:filePath];
        if([self.recentItems isEqual:NULL] || self.recentItems.count==0)
        {
            self.recentItems=[[NSMutableArray alloc]init];
        }
    }
    return  self;
}

@end
