//
//  SecondViewController.h
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *sortedCollection;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end