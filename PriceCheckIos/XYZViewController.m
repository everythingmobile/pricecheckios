//
//  XYZViewController.m
//  SimpleTableTest
//
//  Created by twilight on 04/12/1935 SAKA.
//  Copyright (c) 1935 SAKA twilight. All rights reserved.
//

#import "XYZViewController.h"
#import "Product.h"
#import <ReactiveCocoa/ReactiveCocoa/ReactiveCocoa.h>
#import "ProductRequestClient.h"
#import "MTCollectionViewCell.h"
#import "SecondViewController.h"
#import "ProductManager.h"
#import "ProductViewModel.h"
#import "PopViewController.h"
#import <UIScrollView+SVInfiniteScrolling.h>
#import <UIScrollView+SVPullToRefresh.h>
#import "Utility.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "TSMessage.h"
#import "TSMessageView.h"
#import <SystemConfiguration/SystemConfiguration.h>

#define TRANSITION_DURATION 0.25
@interface XYZViewController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchDisplayField;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic,strong)NSMutableArray *productsArray;
@property (nonatomic,strong)NSMutableArray *autoComplete;
@property (nonatomic,strong) UIActivityIndicatorView *spinner;
-(void)searchForProduct:(NSString *)searchText;
@end

@interface XYZViewController () <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning>
@end
@implementation XYZViewController
{
    NSString *stringToSearch;
    int page;
    BOOL flag;
}
@synthesize productsArray=_productsArray;
@synthesize kenView;

- (void)viewDidLoad {
    NSLog(@"ViewDidLoad!!");
    [super viewDidLoad];
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self addInfiniteScrolling];
    //self.searchBar.barTintColor = [UIColor colorWithRed:103.0/255.0 green:199.0/255.0 blue:195.0/255.0 alpha:1.0];
    //self.view.backgroundColor = [UIColor colorWithRed:103.0/255.0 green:199.0/255.0 blue:195.0/255.0 alpha:1.0];
    self.searchDisplayController.searchBar.translucent = NO;
    [self.kenView setHidden:NO];
    self.kenView.layer.borderWidth = 1;
    flag = YES;
    self.kenView.layer.borderColor = [UIColor blackColor].CGColor;
    self.kenView.delegate = self;
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        [TSMessage showNotificationInViewController:self
                                              title:NSLocalizedString(@"Network Reachability", nil)
                                           subtitle:NSLocalizedString(@"No internet connection!", nil)
                                              image:nil
                                               type:TSMessageNotificationTypeWarning
                                           duration:TSMessageNotificationDurationAutomatic
                                           callback:nil
                                        buttonTitle:nil
                                     buttonCallback:nil
                                         atPosition:TSMessageNotificationPositionBottom
                               canBeDismissedByUser:YES];
    } else {
        NSLog(@"There IS internet connection");
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorHandler:)
                                                 name:@"errorRetrievingProduct"
                                               object:nil];
    //[[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];
}

/*- (UIStatusBarStyle)preferredStatusBarStyle {
    NSLog(@"PreferredStatusBarStyle!!");
    if (self.kenView.hidden == YES ) {
        return UIStatusBarStyleDefault;
    } else {
        return UIStatusBarStyleLightContent;
    }
}*/
-(void) addInfiniteScrolling {
    NSLog(@"AddInfiniteScrolling!!");
    __weak XYZViewController *weakSelf=self;
    
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        ProductManager *sharedManager=[ProductManager sharedManager];
        ProductViewModel *viewModel=[ProductViewModel sharedProductViewModel];
        if([viewModel.productsArray count]>0){
            [[[sharedManager search:stringToSearch page:[NSString stringWithFormat:@"%d",page]]deliverOn:[RACScheduler scheduler]]
             subscribeNext:^(id products) {
                 ProductViewModel *viewModel=[ProductViewModel sharedProductViewModel];
                 [viewModel.productsArray addObjectsFromArray:products];
                 weakSelf.productsArray=[sharedManager filterBySite:viewModel.productsArray];
                 weakSelf.productsArray=[sharedManager filterProducts:weakSelf.productsArray bySuggestion:NO];
                 
                 [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                     [weakSelf.collectionView reloadData];
                     [weakSelf.collectionView.collectionViewLayout invalidateLayout];
                     [weakSelf.collectionView.infiniteScrollingView stopAnimating];
                     
                 }];
                 page++;
             }
             error:^(NSError *error) {
                 [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                     [weakSelf.collectionView.infiniteScrollingView stopAnimating];
                 }];
             }];
        }
        else{
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                [weakSelf.collectionView.infiniteScrollingView stopAnimating];
            }];
        }
        
    }];
}
-(void) errorHandler:(NSNotification *) notification {
    NSLog(@"Control Reached Here!!");
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        if ([[notification name] isEqualToString:@"errorRetrievingProduct"] && flag == YES) {
            [TSMessage showNotificationInViewController:self
                                                  title:NSLocalizedString(@"Something Failed", nil)
                                               subtitle:NSLocalizedString(@"The internet connection seems to be down. Please check that!", nil)
                                                  image:nil
                                                   type:TSMessageNotificationTypeError
                                               duration:TSMessageNotificationDurationAutomatic
                                               callback:nil
                                            buttonTitle:nil
                                         buttonCallback:nil
                                             atPosition:TSMessageNotificationPositionBottom
                                   canBeDismissedByUser:YES];
            flag = NO;
            [self.spinner stopAnimating];
            [self.collectionView setHidden:YES];
            [self.kenView setHidden:NO];
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated {
    NSLog(@"ViewWillAppear!!");
    ProductViewModel *viewModel=[ProductViewModel sharedProductViewModel];
    ProductManager *sharedManager=[ProductManager sharedManager];
    self.productsArray=[sharedManager filterBySite:viewModel.productsArray];
    [self.collectionView reloadData];
    [self.collectionView setBounces:YES];
    [self.collectionView setAlwaysBounceVertical:YES];
    [self.collectionView setAlwaysBounceHorizontal:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"ViewDidAppear!!");
    NSArray *myImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"background.jpg"],nil];
    
    [self.kenView animateWithImages:myImages
                 transitionDuration:12
                               loop:YES
                        isLandscape:YES];
}

- (void)viewDidUnload {
    NSLog(@"ViewDidUnload!!");
    self.kenView.delegate = nil;
    [self setKenView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewDidLayoutSubviews {
    NSLog(@"ViewDidLayoutSubviews!!");
    self.spinner.center = [Utility getDeviceCenter];
    //self.collectionView.contentOffset = CGPointMake(0, 0);
    /*for (UIView *subView in self.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                if (self.kenView.hidden == YES) {
                    searchBarTextField.textColor = [UIColor blackColor];
                    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor blackColor]];
                } else {
                    searchBarTextField.textColor = [UIColor whiteColor];
                    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
                }
                
                break;
            }
        }
    }*/
    if (self.kenView.hidden == YES) {
        /*[self.searchBar setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];*/
        //[self.searchDisplayController.searchResultsTableView setBackgroundColor:[UIColor whiteColor]];
        [self.collectionView setHidden:NO];
    } else {
        //[self.searchBar setTintColor:[UIColor whiteColor]];
        /*[self.searchBar setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];*/
        //[self.searchDisplayController.searchResultsTableView setBackgroundColor:[UIColor whiteColor]];
        [self.collectionView setHidden:YES];
    }
    //[[self.searchBar.subviews objectAtIndex:2] removeFromSuperview];
}
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    //[searchBar setPlaceholder:@""];
    return YES;
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    //[searchBar setPlaceholder:@"Search for Product"];
}
- (void)didReceiveMemoryWarning {
    NSLog(@"DidReceiveMemoryWarning!!");
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"NumberOfSectionInTableView!!");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"NumberOfRowsInSection!!");
    if(tableView==self.searchDisplayController.searchResultsTableView)return self.autoComplete.count;
    return self.productsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"CellForRowAtIndexPath!!");
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell)cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.textLabel.text=self.autoComplete[indexPath.row];
    /*if (self.kenView.hidden == YES) {
        cell.textLabel.textColor=[UIColor blackColor];
    } else {
        cell.textLabel.textColor=[UIColor whiteColor];
    }*/
    cell.backgroundColor=[UIColor clearColor];
    return cell;
   
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"DidSelectRowAtIndexPath!!");
    NSString *searchText=self.autoComplete[indexPath.row];
    [self searchForProduct:searchText];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    NSLog(@"ShouldReloadTableForSearchString!!");
    ProductManager *sharedManager=[ProductManager sharedManager];
    [[sharedManager getAutocompleteResults:searchString]subscribeNext:^(id results) {
        self.autoComplete=results;
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            [self.searchDisplayController.searchResultsTableView reloadData];
        }];
    }];
    return NO;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    NSLog(@"SearchDisplayController");
    return NO;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"SearchBarProductButtonClicked!!");
    //[searchBar setPlaceholder:@"Search for Product"];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        [TSMessage showNotificationInViewController:self
                                              title:NSLocalizedString(@"Network Reachability", nil)
                                           subtitle:NSLocalizedString(@"No internet connection!", nil)
                                              image:nil
                                               type:TSMessageNotificationTypeWarning
                                           duration:TSMessageNotificationDurationAutomatic
                                           callback:nil
                                        buttonTitle:nil
                                     buttonCallback:nil
                                         atPosition:TSMessageNotificationPositionBottom
                               canBeDismissedByUser:YES];
        [self.searchDisplayController setActive:NO animated:YES];
        [self.collectionView setHidden:YES];
        [self.kenView setHidden:NO];
    } else {
        NSLog(@"There IS internet connection");
        [self searchForProduct:searchBar.text];
    }
}

-(void)searchForProduct:(NSString *)searchText {
    NSLog(@"SearchForProduct!!");
    [self.view bringSubviewToFront:self.spinner];
    stringToSearch=searchText;
    [self.spinner startAnimating];
    [self.searchBar resignFirstResponder];
    [self.searchDisplayController setActive:NO];
    [self.kenView setHidden:YES];
    flag = YES;
    ProductManager *sharedManager=[ProductManager sharedManager];
    [[[sharedManager search:searchText page:@"1"]deliverOn:[RACScheduler scheduler]]
     subscribeNext:^(id products) {
         ProductViewModel *viewModel=[ProductViewModel sharedProductViewModel];
         viewModel.productsArray=products;
         viewModel.searchText=searchText;
         if(![viewModel.recentItems containsObject:searchText])
         [viewModel.recentItems insertObject:searchText atIndex:0];
         else
         {
             [viewModel.recentItems removeObject:searchText];
             [viewModel.recentItems insertObject:searchText atIndex:0];
         }
         [sharedManager saveRecentItems];
         self.productsArray=[sharedManager filterBySite:products];
         self.productsArray=[sharedManager filterProducts:self.productsArray bySuggestion:NO];
         [[NSOperationQueue mainQueue]addOperationWithBlock:^{
             [self.spinner stopAnimating];
             [self.collectionView reloadData];
             [self.collectionView.collectionViewLayout invalidateLayout];
             
         }];
         page=2;
     }
     error:^(NSError *error) {
         [[NSOperationQueue mainQueue]addOperationWithBlock:^{
             [self.spinner stopAnimating];
         }];
     }];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSLog(@"NumberOfSectionsInCollectionsView!!");
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"NumberOfItemsInSection:CollectionView!!");
    return self.productsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"CellForItemAtIndexPath:CollectionView!!");
    MTCollectionViewCell *cell = (MTCollectionViewCell *)[collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"Cell"
                                    forIndexPath:indexPath];
    Product *product=self.productsArray[indexPath.row];
    cell.tag=indexPath.row;
    cell.nameLabel.text=product.name;
    
    NSString *price=product.price;
    price=[price stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([price hasPrefix:@"Rs"] == NO) {
        price=[NSString stringWithFormat:@"Rs.%@",price];
    }
    price=[price stringByReplacingOccurrencesOfString:@" " withString:@""];
    cell.priceLabel.text=price;
    cell.priceLabel.textColor = [UIColor redColor];
    cell.productImage.image=nil;
    cell.productImage.contentMode = UIViewContentModeScaleAspectFit;
    if ([product.siteName isEqual:@"homeshop18"])
        cell.siteLogo.contentMode = UIViewContentModeScaleAspectFit;
    else
        cell.siteLogo.contentMode = UIViewContentModeScaleAspectFill;
    cell.siteLogo.image=[UIImage imageNamed:product.siteName];
    NSURLSession *session=[NSURLSession sharedSession];
    NSURLSessionDataTask *task=[session dataTaskWithURL:[NSURL URLWithString:product.imageUrl]
                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if([data length]>0 && error==nil)
                                          {
                                              UIImage *image=[UIImage imageWithData:data];
                                              [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                  if(cell.tag==indexPath.row){
                                                      cell.productImage.image=image;
                                                      product.prodImage=image;
                                                  }
                                              }];
                                          }}];
    [task resume];
    [cell.layer setBorderColor:[UIColor colorWithRed:213.0/255.0f green:210.0/255.0f blue:199.0/255.0f alpha:1.0f].CGColor];
    //[cell.layer setBorderColor:[UIColor greenColor].CGColor];
    [cell.layer setBorderWidth:1.0f];
    [cell.layer setCornerRadius:7.5f];
    //[cell.layer setMasksToBounds:NO];
    //[cell.layer setShadowOffset:CGSizeMake(0, 1)];
    //[cell.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    //[cell.layer setShadowRadius:0.5];
    //[cell.layer setShadowOpacity:0.1];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"SizeForItemAtIndexPath!!");
    // Adjust cell size for orientation
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        return CGSizeMake(155, 247);
    } else {
        if ([Utility getDeviceHeight] == 480) {
            return CGSizeMake(153,247);
        } else {
            return CGSizeMake(133,247);
        }
    }
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)shouldAutorotate {
    NSLog(@"ShouldAutorotate!!");
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    NSLog(@"DidRotateFromInterfaceOrientation!!");
    [self.collectionView performBatchUpdates:nil completion:nil];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"DidSelectItemAtIndexPath:CollectionView!!");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailsView"];
    vc.view.backgroundColor = [UIColor clearColor];
    Product *product = self.productsArray[indexPath.row];
    [vc setCurrentProduct:product];
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    NSLog(@"AnimationControllerForPresentedController!!");
    return self;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    NSLog(@"AnimationControllerForDismissedController!!");
    return self;
}

#pragma mark - UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    NSLog(@"TransitionDuration!!");
    return TRANSITION_DURATION;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    NSLog(@"AnimateTransition!!");
    //    NSLog(@"context class is %@", [transitionContext class]);
    
	//NSIndexPath *selected = self.collectionView.indexPathsForSelectedItems[0];
	//UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:selected];
	
    UIView *container = transitionContext.containerView;
	
	UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *fromView = fromVC.view;
    UIView *toView = toVC.view;
    
    CGRect endFrame = [transitionContext initialFrameForViewController:fromVC];
    CGFloat height = [Utility getDeviceHeight];
    CGFloat width = [Utility getDeviceWidth];
    
    if (toVC.isBeingPresented) {
        toView.frame = CGRectMake(0, height, width, height);
        [container addSubview:toView];
    }
    
    
    [UIView animateWithDuration:TRANSITION_DURATION
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^{
                         if (toVC.isBeingPresented) {
                             toView.frame = endFrame;
                         } else {
                             fromView.frame = CGRectMake(0, height, width, height);
                         }
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition: YES];
                     }
     ];
}

@end

