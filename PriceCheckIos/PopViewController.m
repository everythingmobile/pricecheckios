//
//  PopViewController.m
//  SimpleTableTest
//
//  Created by Pravin Gadakh on 3/8/14.
//  Copyright (c) 2014 twilight. All rights reserved.
//

#import "PopViewController.h"
#import "Utility.h"

@interface PopViewController ()

@end

@implementation PopViewController 

- (IBAction)dismissController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
}

-(void)viewDidLayoutSubviews{
    if ([Utility getDeviceHeight] > 480) {
        self.popupView.center = CGPointMake([Utility getDeviceWidth] / 2.0, [Utility getDeviceHeight] / 2.0);
    }
    [self.popupView.layer setCornerRadius:7.5f];
    [self.popupView.layer setMasksToBounds:YES];
    [self.popupView.layer setShadowOffset:CGSizeMake(0, 1)];
    [self.popupView.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    [self.popupView.layer setShadowRadius:8.0];
    [self.popupView.layer setShadowOpacity:0.8];
    self.productName.text = self.currentProduct.name;
    NSString *price = self.currentProduct.price;
    price = [price stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([price hasPrefix:@"Rs"] == NO) {
        price=[NSString stringWithFormat:@"Rs.%@",price];
    }
    price=[price stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.productPrice.text = price;
    UIImage *image = self.currentProduct.prodImage;
    CGImageRef cgref = [image CGImage];
    CIImage *cim = [image CIImage];
    if (cim == NULL && cgref == NULL) {
        NSLog(@"No Image!!");
        NSURLSession *session=[NSURLSession sharedSession];
        NSURLSessionDataTask *task=[session dataTaskWithURL:[NSURL URLWithString:self.currentProduct.imageUrl]
                                          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if([data length]>0 && error==nil)
                                              {
                                                  UIImage *image=[UIImage imageWithData:data];
                                                  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                      self.currentProduct.prodImage=image;
                                                      self.productImage.image = self.currentProduct.prodImage;
                                                  }];
                                              }}];
        [task resume];
    } else {
        self.productImage.image = self.currentProduct.prodImage;
    }
    self.productImage.contentMode = UIViewContentModeScaleAspectFit;
    if ([self.currentProduct.siteName isEqual:@"homeshop18"])
        self.siteLogo.contentMode = UIViewContentModeScaleAspectFit;
    else
        self.siteLogo.contentMode = UIViewContentModeScaleAspectFill;
    self.siteLogo.image=[UIImage imageNamed:self.currentProduct.siteName];
    [self.visitSiteButton addTarget:self action:@selector(visitSiteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) visitSiteButtonPressed : (id) sender {
    //NSLog(@"%@", self.currentProduct.productLink);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.currentProduct.productLink]];
}

- (BOOL)shouldAutorotate {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
