//
//  Utility.m
//  PriceCheckIos
//
//  Created by Pravin Gadakh on 3/23/14.
//  Copyright (c) 2014 Everythingmobile. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+(UIInterfaceOrientation)getDeviceOrientation {
    return [UIApplication sharedApplication].statusBarOrientation;
}

+(CGFloat)getDeviceWidth {
    CGRect frame = [UIScreen mainScreen].bounds;
    return frame.size.width;
}

+(CGFloat)getDeviceHeight {
    CGRect frame = [UIScreen mainScreen].bounds;
    return frame.size.height;
}

+(CGPoint)getDeviceCenter {
    if (UIDeviceOrientationIsPortrait([self getDeviceOrientation])) {
        return CGPointMake([self getDeviceWidth] / 2.0, [self getDeviceHeight] / 2.0);
    } else {
        return CGPointMake([self getDeviceHeight] / 2.0, [self getDeviceWidth] / 2.0);
    }
}
@end
