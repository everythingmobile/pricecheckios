//
//  FourthViewController.h
//  PriceCheckIos
//
//  Created by Pravin Gadakh on 2/28/14.
//  Copyright (c) 2014 Everythingmobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FourthViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

