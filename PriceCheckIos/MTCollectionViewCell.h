//
//  MTCollectionViewCell.h
//  SimpleTableTest
//
//  Created by twilight on 13/12/1935 SAKA.
//  Copyright (c) 1935 SAKA twilight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTCollectionViewCell : UICollectionViewCell
@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *priceLabel;
@property (nonatomic,weak) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UIImageView *siteLogo;
@end
