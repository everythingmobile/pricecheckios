//
//  ProductViewModel.h
//  PriceCheckIos
//
//  Created by twilight on 17/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductViewModel : NSObject
@property (nonatomic,strong) NSMutableArray *productsArray;
@property (nonatomic,strong) NSMutableDictionary *sitesFilter;
@property (nonatomic,strong) NSMutableArray *recentItems;
@property (nonatomic,strong) NSString *searchText;
+(instancetype)sharedProductViewModel;
@end
