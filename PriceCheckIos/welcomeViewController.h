//
//  welcomeViewController.h
//  PageViewDemo
//
//  Created by Pravin Gadakh on 5/17/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface welcomeViewController : UIViewController
@property NSUInteger pageIndex;
@property (weak, nonatomic) IBOutlet UIImageView *appLogo;
@end
