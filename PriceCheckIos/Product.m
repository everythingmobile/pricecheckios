//
//  Product.m
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import "Product.h"

@implementation Product
-(id)initWithName:(NSString *)name Price:(NSString *)price Site:(NSString *)site ImageUrl:(NSString *)imageUrl ProductLink:(NSString *)productLink AutoSuggest:(BOOL)autosuggested
{
    if(self=[super init])
    {
        self.name=name;
        self.price=price;
        self.siteName=site;
        self.imageUrl=imageUrl;
        self.productLink=productLink;
        self.autoSuggested=autosuggested;
    }
    return self;
}
@end
