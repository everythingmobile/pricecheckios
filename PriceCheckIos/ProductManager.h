//
//  ProductManager.h
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa/ReactiveCocoa.h>
@interface ProductManager : NSObject
+(instancetype) sharedManager;
@property (nonatomic,strong) NSMutableArray *products;
-(RACSignal *) search:(NSString *)name page:(NSString *)page;
-(NSMutableArray *) sortByPrice:(NSMutableArray *)productList withOrder:(int)order;
-(RACSignal *)getAutocompleteResults:(NSString *)name;
-(NSMutableArray *)filterBySite:(NSMutableArray *)productList;
-(NSMutableArray *)filterProducts:(NSMutableArray *)productList bySuggestion:(BOOL)suggested;
-(void) saveRecentItems;
@end
