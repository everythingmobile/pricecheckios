//
//  FourthViewController.m
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import "FourthViewController.h"
#import "MTCollectionViewCell.h"
#import "Product.h"
#import "ProductManager.h"
#import "ProductViewModel.h"
#import "Utility.h"
#import "PopViewController.h"
#define TRANSITION_DURATION 0.25
@interface FourthViewController ()
@property (nonatomic,strong) NSMutableArray *productsArray;
@end

@interface FourthViewController () <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning>
@end
@implementation FourthViewController
ProductViewModel *viewModel;
- (void)viewDidLoad
{
    [super viewDidLoad];
    viewModel=[ProductViewModel sharedProductViewModel];
    [self.infoLabel setHidden:YES];
	// Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    ProductManager *sharedManager=[ProductManager sharedManager];
    ProductViewModel *viewModel=[ProductViewModel sharedProductViewModel];
    self.productsArray=[sharedManager filterProducts:viewModel.productsArray bySuggestion:YES];
    self.productsArray=[sharedManager filterBySite:self.productsArray];
    [self.collection reloadData];
    [self.collection setBounces:YES];
    [self.collection setAlwaysBounceVertical:YES];
    [self.collection setAlwaysBounceHorizontal:YES];
}
-(void)viewDidAppear:(BOOL)animated {
    if (self.productsArray.count == 0) {
        [self.infoLabel setHidden:NO];
    } else {
        [self.infoLabel setHidden:YES];
    }
}
-(void)viewDidLayoutSubviews
{
    self.infoLabel.center = [Utility getDeviceCenter];
    //self.collection.contentOffset = CGPointMake(0, 0);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.productsArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MTCollectionViewCell *cell = (MTCollectionViewCell *)[collectionView
                                                          dequeueReusableCellWithReuseIdentifier:@"Cell"
                                                          forIndexPath:indexPath];
    Product *product=self.productsArray[indexPath.row];
    cell.tag=indexPath.row;
    cell.nameLabel.text=product.name;
    
    NSString *price=product.price;
    price=[price stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([price hasPrefix:@"Rs"] == NO) {
        price=[NSString stringWithFormat:@"Rs.%@",price];
    }
    price=[price stringByReplacingOccurrencesOfString:@" " withString:@""];
    cell.priceLabel.text=price;
    cell.priceLabel.textColor = [UIColor redColor];
    cell.productImage.image=nil;
    cell.productImage.contentMode = UIViewContentModeScaleAspectFit;
    if ([product.siteName isEqual:@"homeshop18"])
        cell.siteLogo.contentMode = UIViewContentModeScaleAspectFit;
    else
        cell.siteLogo.contentMode = UIViewContentModeScaleAspectFill;
    cell.siteLogo.image=[UIImage imageNamed:product.siteName];
    NSURLSession *session=[NSURLSession sharedSession];
    NSURLSessionDataTask *task=[session dataTaskWithURL:[NSURL URLWithString:product.imageUrl]
                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if([data length]>0 && error==nil)
                                          {
                                              UIImage *image=[UIImage imageWithData:data];
                                              [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                  if(cell.tag==indexPath.row)cell.productImage.image=image;
                                              }];
                                          }}];
    [task resume];
    [cell.layer setBorderColor:[UIColor colorWithRed:65.0/255.0f green:218.0/255.0f blue:255.0/255.0f alpha:1.0f].CGColor];
    [cell.layer setBorderWidth:1.0f];
    [cell.layer setCornerRadius:7.5f];
    //[cell.layer setMasksToBounds:NO];
    //[cell.layer setShadowOffset:CGSizeMake(0, 1)];
    //[cell.layer setShadowColor:[[UIColor darkGrayColor] CGColor]];
    //[cell.layer setShadowRadius:8.0];
    //[cell.layer setShadowOpacity:0.8];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Adjust cell size for orientation
    if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        return CGSizeMake(155, 247);
    } else {
        if ([Utility getDeviceHeight] == 480) {
            return CGSizeMake(153,247);
        } else {
            return CGSizeMake(133,247);
        }
    }
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)shouldAutorotate {
    NSLog(@"ShouldAutorotate!!");
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.collection performBatchUpdates:nil completion:nil];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailsView"];
    vc.view.backgroundColor = [UIColor clearColor];
    Product *product = self.productsArray[indexPath.row];
    [vc setCurrentProduct:product];
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    return self;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return self;
}

#pragma mark - UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return TRANSITION_DURATION;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    //    NSLog(@"context class is %@", [transitionContext class]);
    
	//NSIndexPath *selected = self.collectionView.indexPathsForSelectedItems[0];
	//UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:selected];
	
    UIView *container = transitionContext.containerView;
	
	UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *fromView = fromVC.view;
    UIView *toView = toVC.view;
    
    CGRect endFrame = [transitionContext initialFrameForViewController:fromVC];
    CGFloat height = [Utility getDeviceHeight];
    CGFloat width = [Utility getDeviceWidth];
    
    if (toVC.isBeingPresented) {
        toView.frame = CGRectMake(0, height, width, height);
        [container addSubview:toView];
    }
    
    
    [UIView animateWithDuration:TRANSITION_DURATION
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^{
                         if (toVC.isBeingPresented) {
                             toView.frame = endFrame;
                         } else {
                             fromView.frame = CGRectMake(0, height, width, height);
                         }
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition: YES];
                     }
     ];
}

@end
