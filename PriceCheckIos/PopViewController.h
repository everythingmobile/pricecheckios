//
//  PopViewController.h
//  SimpleTableTest
//
//  Created by Pravin Gadakh on 3/8/14.
//  Copyright (c) 2014 twilight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface PopViewController : UIViewController
@property (strong,nonatomic) Product * currentProduct;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UIImageView *siteLogo;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *productPrice;
@property (weak, nonatomic) IBOutlet UIButton *visitSiteButton;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@end
