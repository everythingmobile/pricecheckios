//
//  Product.h
//  PriceCheckIos
//
//  Created by twilight on 07/12/1935 SAKA.
//  Copyright (c) 1935 SAKA Everythingmobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *siteName;
@property (nonatomic,strong) NSString *imageUrl;
@property (nonatomic,strong) NSString *productLink;
@property (nonatomic,strong) UIImage *prodImage;
@property BOOL autoSuggested;
-(id) initWithName:(NSString *)name Price:(NSString *)price Site:(NSString *)site ImageUrl:(NSString *)imageUrl ProductLink:(NSString *)productLink AutoSuggest:(BOOL)autosuggested;
@end
